package br.com.merge.model;

import java.util.Date;

/**
 *Classe que repesenta um Candidato
 *@author Henrique Cesar
 *@author Dennys Nascimenro 
 *@author Luan Reis
 *@author Gustavo Fonseca
 *@author Rodrigo Machado
 *
 */
public class Candidato {
	
	
	private int codigo;

	/**
	 *Armazena o nome, cpf, estado civil, sexo, email e senha  
	 */
    private String nome, cpf, dsEstadoCivil, dsSexo, dsEmail, psSenhaLogin;
    
    /**
     * Armazena a data de nascimento
     */
    private String dtNascimento;
    
    /**
     * Armazena o currículo
     */
    private Curriculo curriculo;
    
    /**
     * Armazena o endereco
     */
    private Endereco endereco;
    
    /**
     * Armazena o telefone
     */
    private Telefone telefone;

    /**
     * Construtor padrão
     */
    public Candidato(){
    }

    /**
     * Construtor que recebe o nome, cpf, estado civil, sexo, email, senha, data de nascimento, curriculo, endereco e telefone do candidato
     * @param nome de um candidato
     * @param cpf de um candidato
     * @param dsEstadoCivil de um candidato
     * @param dsSexo de um candidato
     * @param dsEmail de um candidato
     * @param psSenhaLogin de um candidato
     * @param dtNascimento de um candidato
     * @param curriculo de um candidato
     * @param endereco de um candidato
     * @param telefone de um candidato
     */
	public Candidato(int codigo, String nome, String cpf, String dsEstadoCivil, String dsSexo, String dsEmail, String psSenhaLogin,
			String dtNascimento, Curriculo curriculo, Endereco endereco, Telefone telefone) {
		this.codigo = codigo;
		this.nome = nome;
		this.cpf = cpf;
		this.dsEstadoCivil = dsEstadoCivil;
		this.dsSexo = dsSexo;
		this.dsEmail = dsEmail;
		this.psSenhaLogin = psSenhaLogin;
		this.dtNascimento = dtNascimento;
		this.curriculo = curriculo;
		this.endereco = endereco;
		this.telefone = telefone;
	}
	
	
	
	

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		codigo = codigo;
	}

	/**
	 * Retorna o nome do candidato
	 * @return nomde candidato
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Altera o nome do candidato
	 * @param nome do candidato
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Retorna o cpf do candidato
	 * @return cpf do candidato
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Altera o cpf do candidato
	 * @param cpf do candidato
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Retorna o estado civil do candidato
	 * @return estado civil do candidato
	 */
	public String getDsEstadoCivil() {
		return dsEstadoCivil;
	}

	/**
	 * Altera o estado civil do candidato
	 * @param estado civil do candidato
	 */
	public void setDsEstadoCivil(String dsEstadoCivil) {
		this.dsEstadoCivil = dsEstadoCivil;
	}

	/**
	 * Retorna o sexo do candidato
	 * @return sexo do candidato
	 */
	public String getDsSexo() {
		return dsSexo;
	}

	/**
	 * Altera o sexo do candidato
	 * @param sexo do candidato
	 */
	public void setDsSexo(String dsSexo) {
		this.dsSexo = dsSexo;
	}

	/**
	 * Retorna o email do candidato
	 * @return email do candidato
	 */
	public String getDsEmail() {
		return dsEmail;
	}

	/**
	 * Altera o email do candidato
	 * @param email do candidato
	 */
	public void setDsEmail(String dsEmail) {
		this.dsEmail = dsEmail;
	}

	/**
	 * Retorna a senha do candidato
	 * @return senha do candidato
	 */
	public String getPsSenhaLogin() {
		return psSenhaLogin;
	}

	/**
	 * Altera a senha do candidato
	 * @param senha do candidato
	 */
	public void setPsSenhaLogin(String psSenhaLogin) {
		this.psSenhaLogin = psSenhaLogin;
	}

	/**
	 * Retorna a data de nascimento do candidato
	 * @return data de nascimento candidato
	 */
	public String getDtNascimento() {
		return dtNascimento;
	}

	/**
	 * Altera a data de nascimento do candidato
	 * @param data de nascimento do candidato
	 */
	public void setDtNascimento(String dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	/**
	 * Retorna um curriculo do candidato
	 * @return um curriculo do candidato
	 */
	public Curriculo getCurriculo() {
		return curriculo;
	}

	/**
	 * Altera o curriculo do candidato
	 * @param curriculo do candidato
	 */
	public void setCurriculo(Curriculo curriculo) {
		this.curriculo = curriculo;
	}

	/**
	 * Retorna o endereco do candidato
	 * @return endereco do candidato
	 */
	public Endereco getEndereco() {
		return endereco;
	}
	
	/**
	 * Altera o endereco do candidato
	 * @param endereco do candidato
	 */
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	/**
	 * Retorna o telefone do candidato
	 * @return telefone do candidato
	 */
	public Telefone getTelefone() {
		return telefone;
	}

	/**
	 * Altera o telefone do candidato
	 * @param telefone do candidato
	 */
	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}

}