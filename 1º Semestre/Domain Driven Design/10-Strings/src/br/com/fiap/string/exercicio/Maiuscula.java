package br.com.fiap.string.exercicio;

import javax.swing.JOptionPane;

public class Maiuscula {

	public static void main(String[] args) {
		
		//Ler uma palavra
		String palavra = JOptionPane.showInputDialog("Digite uma palavra");
		
		//Exibir a palavra em mai�scula
		JOptionPane.showMessageDialog(null, palavra.toUpperCase());
		
	}
}
