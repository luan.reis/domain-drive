package br.com.fiap.string.view;

public class ExemploComparacao {

	public static void main(String[] args) {
		
		String praia = "Ilhabela";
		String praia2 = "ilhabela";
		
		//Comparar as duas strings -> equals para comprar o conte�do da string
		if (praia.equals(praia2)) {
			System.out.println("Iguais");
		} else {
			System.out.println("Diferentes");
		}
		
		//Comparar duas string sem considerar o case (mai�sculas e min�sculas)
		if (praia.equalsIgnoreCase(praia2)) {
			System.out.println("Iguais");
		} else {
			System.out.println("Diferentes");
		}
		
	}
}