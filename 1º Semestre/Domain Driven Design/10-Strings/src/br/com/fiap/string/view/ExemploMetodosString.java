package br.com.fiap.string.view;

public class ExemploMetodosString {

	public static void main(String[] args) {
		//Declarar uma string
		String estado = "Rio Grande do Sul";
		
		//Exibir a quantidade de caracteres
		int qtd = estado.length();
		System.out.println(estado + " tem " + qtd + " caracteres");
		
		//Recuperar o caractere da 7 posi��o do estado e exibir o caractere
		char letra = estado.charAt(6);
		System.out.println("A s�tima letra do estado � \"" + letra + "\"");
		
		//pegou o estado, substituiu o espa�o por nada e calculou a qtd de caracteres 
		qtd = estado.replace(" ", "").length();
		System.out.println("Caracteres: " + qtd); 

		//Recuperar a posi��o do primeiro caractere "a" da string
		int posicao = estado.indexOf("a");
		System.out.println("A primeira letra \"a\" est� na posi��o: " + posicao);
		
		//Recuperar o primeiro nome do estado
		String primeiroNome = estado.substring(0, estado.indexOf(" "));
		
		//Exibir o primeiro nome
		System.out.println(primeiroNome);
		
		//Recuperar o �ltimo nome do estado
		String ultimoNome = estado.substring(estado.lastIndexOf(" ") + 1);
		
		//Exibir o �ltimo nome
		System.out.println(ultimoNome);
		
		//Exibir o nome do estado com todas as letras em mai�sculas
		String estadoMaiusculo = estado.toUpperCase();
		System.out.println(estadoMaiusculo);
		
		//Exibir o nome do estado com todas as letras em min�sculas
		System.out.println(estado.toLowerCase());
		
		//Exibir o nome do estado trocando o "Sul" por "Norte"
		System.out.println(estado.replace("Sul", "Norte"));
		
	}
}