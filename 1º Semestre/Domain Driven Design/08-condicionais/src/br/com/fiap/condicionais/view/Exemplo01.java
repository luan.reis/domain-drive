package br.com.fiap.condicionais.view;

import javax.swing.JOptionPane;

public class Exemplo01 {

	public static void main(String[] args) {
		
		
		int numero = Integer.parseInt(JOptionPane.showInputDialog("Digite um numero: "));
		
		if(numero % 2 == 0) {
			JOptionPane.showMessageDialog(null, numero + " � PAR" );
		}else {
			JOptionPane.showMessageDialog(null, numero +" � IMPAR");
		}
		
		
		int idade = Integer.parseInt(JOptionPane.showInputDialog("Digite a sua idade"));
		// 16 - 18 ou maior de 65 � opcional
		if(idade >= 16 && idade < 18 || idade >= 65 ) {
			JOptionPane.showMessageDialog(null, "Voc� tem " + idade + " e seu voto � OPCIONAL!");
		}else if(idade >= 18) {
			JOptionPane.showMessageDialog(null, "Voc� tem " + idade + " e seu voto � OBRIGAT�RIO!");
		}else {
			JOptionPane.showMessageDialog(null, "Voc� tem " + idade + " e voc� N�O PODE votar!");
		}
		
		
		
	}
	
}
