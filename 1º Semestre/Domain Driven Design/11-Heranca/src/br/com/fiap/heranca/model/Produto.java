package br.com.fiap.heranca.model;

// Object <- Produto
// Todas as classes herdam de Object (direta ou indiretamente)
public class Produto {

	// Atributos
	private int codigo;
	private String titulo;
	protected String material;
	private double preco;
	
	//Construtor
	public Produto(int codigo, String titulo, String material, double preco) {
		this.codigo = codigo;
		this.titulo = titulo;
		this.material = material;
		this.preco = preco;
	}
	
	//M�todo
	public double calcularImposto() {
		//Calcular o imposto
		return preco * 0.45;
	}

	//Getters e Setters
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

}