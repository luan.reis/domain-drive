package br.com.fiap.tds.ltp.ex2.produtos.livro;

import br.com.fiap.tds.ltp.ex2.produtos.Produto;

public class Livro extends Produto {

	private String titulo, autor, isbn;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
}
