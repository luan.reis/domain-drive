package br.com.fiap.exercicio.model;

public class Lancha extends Transporte {

	private int quantidadeMotores;
	
	public Lancha(String modelo, int quantidadeLugares, double comprimento, int anoFabricacao, Cor cor,
			int quantidadeMotores) {
		super(modelo, quantidadeLugares, comprimento, anoFabricacao, cor);
		this.quantidadeMotores = quantidadeMotores;
	}
	
	public Lancha() {}

	public int getQuantidadeMotores() {
		return quantidadeMotores;
	}

	public void setQuantidadeMotores(int quantidadeMotores) {
		this.quantidadeMotores = quantidadeMotores;
	}
	
}