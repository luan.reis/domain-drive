package br.com.fiap.polimorfismo.model;

public class Produto {

	private String titulo;
	
	protected double preco;


	
	public Produto(String titulo, double preco) {
		this.titulo = titulo;
		this.preco = preco;
	}
	
	public Produto() {
		
	}
	
	

	@Override
	public String toString() {
		return titulo + " " + preco;
	}
	
	//M�todos
	// Criar um m�todo quesobrescer o calcularDesconto e recebe um cupom(Sring)
	public double calcularDesconto(String cupom) {
		cupom = cupom.toUpperCase();
		if(cupom.equals("FIAP20" )) {
			return preco * 0.8;
		}
		else if(cupom.equals("FIAP30")) {
			return preco * 0.7;
		}
			return preco;
	}
	
	//Retorna o pre�o do produto com o desconto
	public double calcularDesconto() {
		return preco * 0.95; //valor padr�o de desconto
	}
	
	//Sobrecarga -> m�todo com o mesmo nome, mas com par�metros diferentes
	//Retorna o pre�o do produto com o desconto (10, 20)
	public double calcularDesconto(int porcentagem) {
		return preco * (100-porcentagem)/100;
	}
	
	//Par�metros diferentes -> no tipo, na quantidade ou na ordem
	//Retorna o pre�o do produto com o desconto (10 reais, 20 reais)
	public double calcularDesconto(double valor) {
		return preco - valor;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}
	
}