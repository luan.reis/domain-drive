package br.com.fiap.polimorfismo.view;

import br.com.fiap.polimorfismo.model.Alimento;
import br.com.fiap.polimorfismo.model.Produto;

public class View {
	
	public static void main(String[] args) {
		//Instanciar um produto
		Produto produto = new Produto();
		String real = "R$";
		//Setar um pre�o
		produto.setPreco(1000);
		
		
		//Chamar os m�todos calcularDesconto 
		
		 double desconto1 =  produto.calcularDesconto();
		 double desconto2 =  produto.calcularDesconto(250.0);
		 double desconto3 =  produto.calcularDesconto(20);
		
		//Exibir o resultado do c�lculo
		 
		 System.out.println("Desconto 1: " + real + desconto1);
		 System.out.println("Desconto 2: " + real + desconto2);
		 System.out.println("Desconto 3: " + real + desconto3);
		 
		 
		 //Instanciar um alimento
		 Alimento alimento = new Alimento();
		 
		 alimento.setPreco(100);
		 
		 //Chamar os m�todos calculardesconto e exibir o resultado de calculo
		 
		 System.out.println("\n Alimento \n");
		 System.out.println("Desconto 1: "+ real + alimento.calcularDesconto());
		 System.out.println("Desconto 2: "+ real + alimento.calcularDesconto(50.0));
		 System.out.println("Desconto 3: "+ real + alimento.calcularDesconto(10));
		 
		 System.out.println("\n Cupom desconto \n");
		 //chamar o met�do com cupom de desconto
		 System.out.println("Desconto 1: "+ real + alimento.calcularDesconto("fiap20"));
		 System.out.println("Desconto 2: "+ real + alimento.calcularDesconto("fiap30"));
		 System.out.println("Desconto 6: "+ real + alimento.calcularDesconto("fiap40"));
		
		 
		 
		 Produto arroz =  new Alimento();
		 arroz.setPreco(10);
		 System.out.println(arroz.calcularDesconto("fiap40"));
	}
}
