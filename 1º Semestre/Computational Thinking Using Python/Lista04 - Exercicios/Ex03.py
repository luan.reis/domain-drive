# Altere o algoritmo anterior para, além da média, contar os alunos que tiraram entre 0 e 5, 0
# (0 ≤ nota < 5, 0) e acima de 5, 0 (nota ≥ 5, 0)

numAlunos = int(input("Digite a quantidade alunos: "))
i = 1;
media = 0;
nota = 0;
maior = 0;
menor = 0;

while i <= numAlunos:
    nota = float(input(f"Digite a nota do  {i}º Aluno: "))
    if nota >= 5:
        maior += 1;
    else:
        menor += 1;
    media += nota;
    i+= 1;

print("A média dos {} alunos foi de: {:.2f}".format(numAlunos, (media / numAlunos)))
print("(Quantidade de alunos que tiraram maior que 5:", maior,")" )
print("(Quantidade de alunos que tiraram menor que 5:", menor,")" )

