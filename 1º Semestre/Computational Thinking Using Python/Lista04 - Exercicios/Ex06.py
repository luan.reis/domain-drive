# 6. Em uma prova de concurso com 70 questões haviam 20 pessoas concorrendo. Sabendo que
# cada questão vale 1 ponto, escreva um algoritmo que lê a pontuação da prova obtida de cada
# um dos candidatos e calcula:
# a) a maior e a menor nota
# b) o percentual de candidatos que acertaram até 20 questões, o percentual que acertaram
# de 21 a 50 e o percentual que acertou acima de 50 questões

candidatos = 20;
nota = 0;
maior = 0;
menor = 0;
pmedia = 0;
pmenor = 0;
pmax = 0;
i = 1;

while i <= 20:
    nota = float(input("Digite a pontuação do {}º candidato: ".format(i)))

    if nota > 50:
        pmax += 1;
        if nota > maior:
            maior = nota;
    elif nota >= 21 and nota <= 50:
        pmedia += 1;
    else:
        pmenor +=1;
        if nota < menor:
            menor = nota;
    i += 1;

    
print("Maior nota: {} ".format(maior))
print("Percentual que acertaram até 20 questões: {}%".format(round((pmenor / 20) * 100)))
print("Percentual que acertaram entre 21 e 50 questões: {}%".format(round((pmedia / 20) * 100)))
print("Percentual que acertaram mais que 50 questões: {}%".format(round((pmax / 20) * 100)))