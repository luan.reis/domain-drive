# Dados o número n de alunos de uma turma de Algoritmos e suas notas da primeira prova,
# determinar a média das notas dessa turma. Considere que o usuário digite as informações
# corretamente.

numAlunos = int(input("Digite a quantidade alunos: "))
i = 1;
media = 0;


while i <= numAlunos:
    media += float(input(f"Digite a nota do  {i}º Aluno: "))
    i+= 1;


print("A média dos", numAlunos, "alunos foi de: ",round((media / numAlunos)))






