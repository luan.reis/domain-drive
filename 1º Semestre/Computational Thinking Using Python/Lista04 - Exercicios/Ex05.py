# Escreva um algoritmo que, dados um número inteiro positivo n, imprime na tela a contagem
# de todos os divisores positivos de n.

n = int(input("Digite um numero inteiro positivo: "))
i = 1;

while i < n:
    if n > 0:
        if n % i == 0:
            print(i, end=", ")
    i+= 1;

print(n, end="")       