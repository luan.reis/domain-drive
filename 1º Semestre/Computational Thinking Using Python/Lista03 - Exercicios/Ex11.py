preco = float(input("informe o valor do produto: "))
escolha = int(input("Informe a forma de pagamento: "))

if escolha == 1:
    print("Desconto de 10%: valor final: R${:.2f}".format(preco * 0.9))
elif escolha == 2:
    print("Desconto de 5% valor final: R${:.2f}".format(preco * 0.95))
elif escolha == 3:
    print("2 vezes ")
    print("Valor: {:.2f}".format( preco / 2))
elif escolha == 4:
    print("4 vezes com juros de 7%")
    print("Valor: R${:.2f}".format(preco * 1.07 / 4))
else:
    print("Opção de pagamento inválida!")