from xmlrpc.client import boolean


diasU = int(input("Informe a quantidade de dias úteis do mês: "))
hTrab = float(input("Informe a quantidade de horas trabalhada no mês: "))
valorH = float(input("Informe o valor por hora que você recebe: "))
salarioExtra = 0;


horarioNormal = diasU * 8;

if horarioNormal <  hTrab:
    salarioExtra = hTrab - horarioNormal;
    salarioExtra = salarioExtra * (valorH * 1.50);
    
    
print("==================== D E M O N S T R A T I V O ====================")
print("Salario mensal: R${:.2f}".format(horarioNormal * valorH ))
print("Horas extra: R${:.2f}".format(salarioExtra))
print("Sálario total: R${:.2f}".format(salarioExtra + (horarioNormal * valorH)))

