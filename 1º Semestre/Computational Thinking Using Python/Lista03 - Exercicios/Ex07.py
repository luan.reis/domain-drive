import math


num1 = int(input("Informe um numero para descobrir a raiz quadrada: "))

print("A raiz quadrada de {} é {:.4f}".format(num1 , math.sqrt(num1)))