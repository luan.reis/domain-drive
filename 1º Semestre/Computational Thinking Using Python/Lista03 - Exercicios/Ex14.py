from distutils.log import fatal

ano = int(input("Informe ano: "))

if ano % 4 == 0 and ano % 100 != 0 or ano % 400 == 0:
    print("Ano bissexto!")
else:
    fatal("Não é ano bissexto!")