def organiza(lista, pos):
    aux = lista[pos]
    while pos > 0 and lista[pos-1] > aux:
        lista[pos] = lista[pos-1]
        pos = pos - 1

    lista[pos] = aux

vetor = [44, 30, 18, 23, 9, 16, 5, 4, 0]
for i in range(1, len(vetor)):
    organiza(vetor, i)
print(vetor)

