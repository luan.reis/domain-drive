import math
from statistics import median


def desvioPadrao(dados):
    media = 0
    soma = 0

    for med in dados:
        media += med
    media = media / len(dados)

    for sum in dados:
        soma += (sum - media) ** 2

    somat = soma / (len(dados) - 1) 
    somat = math.sqrt(somat)
    
    return round(somat, 2)


dados = (0, 10, 0, 10, 0, 10)
print("O desvio padrao:", desvioPadrao(dados))