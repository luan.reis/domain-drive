# Faça uma função que recebe uma tupla de números inteiros e retorna o maior valor contido
# dentro da tupla.
def verifMaior(dados):
    maior = dados[0]

    for num in dados:
        if(num >= maior):
            maior = num
    return maior

conjunto = (7,2,3,5,6,7,8,4,2,23,32,123)
print(verifMaior(conjunto))



