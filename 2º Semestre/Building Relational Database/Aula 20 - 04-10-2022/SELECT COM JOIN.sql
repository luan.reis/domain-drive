//SELECIONANDO OS ATRIBUTOS DA TABELSA ESTADO;

SELECT ID.ESTADO, 
	   SG_ESTADO,
	   NM_ESTADO
FROM T_RHSTU_ESTADO
ORDER BY SG_ESTADO;	
---
---SELECIONANDO E JUNTANDO TABELAS
---

-- ERRO AMBIGUOUSLY - NAO FAZER ASSIM
SELECT ID_ESTADO, 
	   SG_ESTADO,
	   NM_ESTADO,
	   ID_CIDADE,
	   ID_ESTADO,
	   NM_CIDADE,
	   CD_IBGE,
	   NR_DDD
FROM T_RHSTU_ESTADO,
	 T_RHSTU_CIDADE
ORDER BY SG_ESTADO;

--
--- SELECIONANDO SEM DAR O ERRO ACIMA
--
SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD
FROM T_RHSTU_ESTADO E,
	 T_RHSTU_CIDADE C
ORDER BY E.SG_ESTADO;

--
-- PRIMEIRO JOIN (INNER JOIN)
-- É SEMPRE RECOMENDADO FAZER JOIN COM CHAVE PRIMARIA E CHAVE ESTRANGEIRA

SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD
FROM T_RHSTU_ESTADO E INNER JOIN T_RHSTU_CIDADE C
ON (E.ID_ESTADO = C.ID_ESTADO)
ORDER BY E.SG_ESTADO, C.NM_CIDADE;

--
-- (LEFT JOIN)
-- É SEMPRE RECOMENDADO FAZER JOIN COM CHAVE PRIMARIA E CHAVE ESTRANGEIRA

SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD
FROM T_RHSTU_ESTADO E LEFT JOIN T_RHSTU_CIDADE C
ON (E.ID_ESTADO = C.ID_ESTADO)
ORDER BY E.SG_ESTADO, C.NM_CIDADE;

---
-- (RIGHT JOIN)
-- É SEMPRE RECOMENDADO FAZER JOIN COM CHAVE PRIMARIA E CHAVE ESTRANGEIRA

SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD
FROM T_RHSTU_ESTADO E RIGHT JOIN T_RHSTU_CIDADE C
ON (E.ID_ESTADO = C.ID_ESTADO)
ORDER BY E.SG_ESTADO, C.NM_CIDADE;

---
-- (INNER JOIN) * PADRAO ORACLE
-- É SEMPRE RECOMENDADO FAZER JOIN COM CHAVE PRIMARIA E CHAVE ESTRANGEIRA

SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD
FROM T_RHSTU_ESTADO E,T_RHSTU_CIDADE C
WHERE (E.ID_ESTADO = C.ID_ESTADO)
ORDER BY E.SG_ESTADO, C.NM_CIDADE;


---
-- (OUTER JOIN) * PADRAO ORACLE
-- É SEMPRE RECOMENDADO FAZER JOIN COM CHAVE PRIMARIA E CHAVE ESTRANGEIRA

SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD
FROM T_RHSTU_ESTADO E,T_RHSTU_CIDADE C
WHERE E.ID_ESTADO = C.ID_ESTADO(+)
ORDER BY E.SG_ESTADO, C.NM_CIDADE;



--
-- (LEFT JOIN)
-- Exibir somente os estados que nao possuem cidade

SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD
FROM T_RHSTU_ESTADO E LEFT JOIN T_RHSTU_CIDADE C
ON (E.ID_ESTADO = C.ID_ESTADO)
WHERE C.ID_ESTADO IS NULL
ORDER BY E.SG_ESTADO, C.NM_CIDADE;




--
-- (LEFT JOIN)
-- exibir os dados do estado, cidade e bairro

SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD,
	   B.NM_BAIRRO,
	   B.NM_ZONA_BAIRRO
FROM T_RHSTU_ESTADO E LEFT JOIN T_RHSTU_CIDADE C
ON (E.ID_ESTADO = C.ID_ESTADO) LEFT JOIN T_RHSTU_BAIRRO B
ON (C.ID_CIDADE = B.ID_CIDADE)
ORDER BY E.SG_ESTADO, C.NM_CIDADE, B.NM_BAIRRO;


--
-- (LEFT JOIN)
-- exibir os dados do estado, cidade , bairro, logradouro 

SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD,
	   B.NM_BAIRRO,
	   B.NM_ZONA_BAIRRO,
	   L.NM_LOGRADOURO,
	   L.NR_CEP
FROM T_RHSTU_ESTADO E LEFT JOIN T_RHSTU_CIDADE C
ON (E.ID_ESTADO = C.ID_ESTADO) LEFT JOIN T_RHSTU_BAIRRO B
ON (C.ID_CIDADE = B.ID_CIDADE) LEFT JOIN T_RHSTU_LOGRADOURO L
ON (B.ID_BAIRRO = L.ID_BAIRRO)
ORDER BY E.SG_ESTADO, C.NM_CIDADE, B.NM_BAIRRO, L.NM_LOGRADOURO;



--
-- (LEFT JOIN)
-- exibir os dados do estado, cidade , bairro, logradouro E endereço paciente

SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD,
	   B.NM_BAIRRO,
	   B.NM_ZONA_BAIRRO,
	   L.NM_LOGRADOURO,
	   L.NR_CEP,
	   EP.NR_LOGRADOURO,
	   EP.DS_PONTO_REFERENCIA,
	   EP.DT_INICIO,
	   EP.DT_FIM
FROM T_RHSTU_ESTADO E LEFT JOIN T_RHSTU_CIDADE C
ON (E.ID_ESTADO = C.ID_ESTADO) LEFT JOIN T_RHSTU_BAIRRO B
ON (C.ID_CIDADE = B.ID_CIDADE) LEFT JOIN T_RHSTU_LOGRADOURO L
ON (B.ID_BAIRRO = L.ID_BAIRRO) LEFT JOIN T_RHSTU_ENDERECO_PACIENTE EP
ON (L.ID_LOGRADOURO =  EP.ID_LOGRADOURO)
ORDER BY E.SG_ESTADO, C.NM_CIDADE, B.NM_BAIRRO;



--
-- (LEFT JOIN)
-- exibir os dados do estado, cidade , bairro, logradouro, endereço paciente e nome do paciente

SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD,
	   B.NM_BAIRRO,
	   B.NM_ZONA_BAIRRO,
	   L.NM_LOGRADOURO,
	   L.NR_CEP,
	   EP.NR_LOGRADOURO,
	   EP.DS_PONTO_REFERENCIA,
	   EP.DT_INICIO,
	   EP.DT_FIM,
	   P.NM_PACIENTE,
	   P.DT_NASCIMENTO,
	   P.DS_ESTADO_CIVIL
FROM T_RHSTU_ESTADO E LEFT JOIN T_RHSTU_CIDADE C
ON (E.ID_ESTADO = C.ID_ESTADO) LEFT JOIN T_RHSTU_BAIRRO B
ON (C.ID_CIDADE = B.ID_CIDADE) LEFT JOIN T_RHSTU_LOGRADOURO L
ON (B.ID_BAIRRO = L.ID_BAIRRO) LEFT JOIN T_RHSTU_ENDERECO_PACIENTE EP
ON (L.ID_LOGRADOURO = EP.ID_LOGRADOURO) LEFT JOIN T_RHSTU_PACIENTE P
ON (EP.ID_PACIENTE =  P.ID_PACIENTE)
ORDER BY E.SG_ESTADO, C.NM_CIDADE, B.NM_BAIRRO, P.NM_PACIENTE;



--
-- (LEFT JOIN) EXIBIR SOMENTE OS PACIENTES QUE TEM ENDENRECO
-- exibir os dados do estado, cidade , bairro, logradouro, endereço paciente e nome do paciente

SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD,
	   B.NM_BAIRRO,
	   B.NM_ZONA_BAIRRO,
	   L.NM_LOGRADOURO,
	   L.NR_CEP,
	   EP.NR_LOGRADOURO,
	   EP.DS_PONTO_REFERENCIA,
	   EP.DT_INICIO,
	   EP.DT_FIM,
	   P.NM_PACIENTE,
	   P.DT_NASCIMENTO,
	   P.DS_ESTADO_CIVIL
FROM T_RHSTU_ESTADO E INNER JOIN T_RHSTU_CIDADE C
ON (E.ID_ESTADO = C.ID_ESTADO) INNER JOIN T_RHSTU_BAIRRO B
ON (C.ID_CIDADE = B.ID_CIDADE) INNER JOIN T_RHSTU_LOGRADOURO L
ON (B.ID_BAIRRO = L.ID_BAIRRO) INNER JOIN T_RHSTU_ENDERECO_PACIENTE EP
ON (L.ID_LOGRADOURO = EP.ID_LOGRADOURO) INNER JOIN T_RHSTU_PACIENTE P
ON (EP.ID_PACIENTE =  P.ID_PACIENTE)
ORDER BY E.SG_ESTADO, C.NM_CIDADE, B.NM_BAIRRO, P.NM_PACIENTE;



---
--- CRIANDO UMA VIEW LOGICA
---

CREATE OR REPLACE VIEW V_RHSTU_DADOS_PACIENTE AS
SELECT E.ID_ESTADO, 
	   E.SG_ESTADO,
	   E.NM_ESTADO,
	   C.ID_CIDADE,
	   C.ID_ESTADO ID_ESTADO_CIDADE,
	   C.NM_CIDADE,
	   C.CD_IBGE,
	   C.NR_DDD,
	   B.NM_BAIRRO,
	   B.NM_ZONA_BAIRRO,
	   L.NM_LOGRADOURO,
	   L.NR_CEP,
	   EP.NR_LOGRADOURO,
	   EP.DS_PONTO_REFERENCIA,
	   EP.DT_INICIO,
	   EP.DT_FIM,
	   P.NM_PACIENTE,
	   P.DT_NASCIMENTO,
	   P.DS_ESTADO_CIVIL
FROM T_RHSTU_ESTADO E INNER JOIN T_RHSTU_CIDADE C
ON (E.ID_ESTADO = C.ID_ESTADO) INNER JOIN T_RHSTU_BAIRRO B
ON (C.ID_CIDADE = B.ID_CIDADE) INNER JOIN T_RHSTU_LOGRADOURO L
ON (B.ID_BAIRRO = L.ID_BAIRRO) INNER JOIN T_RHSTU_ENDERECO_PACIENTE EP
ON (L.ID_LOGRADOURO = EP.ID_LOGRADOURO) INNER JOIN T_RHSTU_PACIENTE P
ON (EP.ID_PACIENTE =  P.ID_PACIENTE)
ORDER BY E.SG_ESTADO, C.NM_CIDADE, B.NM_BAIRRO, P.NM_PACIENTE;




