set pagesize 0
select 'Estado( 27 linhas)..........=> ' || count(*) Verifica_Estado from t_rhstu_estado;
select 'Cidade(130 linhas)..........=> ' || count(*) Verifica_Cidade from t_rhstu_cidade;
select 'Bairro(260 linhas)..........=> ' || count(*) Verifica_Bairro from t_rhstu_bairro;
select 'Logradouro(260 linhas)......=> ' || count(*) Verifica_Logradouro from t_rhstu_logradouro;
select 'Tipo Contato(6 linhas)......=> ' || count(*) Verifica_Contato from t_rhstu_tipo_contato;
select 'Paciente(130 linhas)........=> ' || count(*) Verifica_Paciente from t_rhstu_paciente;
select 'LogrPaciente(130 linhas)....=> ' || count(*) Verifica_LogrPaciente from t_rhstu_endereco_paciente;
select 'ContatoPaciente(130 linhas).=> ' || count(*) Verifica_ContatoPaciente from t_rhstu_contato_paciente;
select 'Hospital(1 linha)...........=> ' || count(*) Verifica_Hospital from t_rhstu_unid_hospitalar;
select 'Médico(20 linhas)...........=> ' || count(*) Verifica_Medico from t_rhstu_medico;
select 'Medicamento(20 linhas)......=> ' || count(*) Verifica_Medicamento from t_rhstu_medicamento;
select 'ConsultaMedica(130 linhas)..=> ' || count(*) Verifica_Consulta from t_rhstu_consulta;
select 'Prescrição(130 linhas)......=> ' || count(*) Verifica_PrescricaoMed from t_rhstu_prescicao_medica;
select 'TelefonePac(260 linhas).....=> ' || count(*) Verifica_TelefonePaciente from t_rhstu_telefone_paciente;
select 'EmailPaciente(260 linhas)...=> ' || count(*) Verifica_EmailPaciente from t_rhstu_email_paciente;
select 'PlanoSaude(3 linhas)........=> ' || count(*) Verifica_PlanoSaude from t_rhstu_plano_saude;
select 'PacPlanoSaude(100 linhas)...=> ' || count(*) Verifica_PacientePlanoSaude from t_rhstu_paciente_plano_saude;
select 'FormaPagto(6 linhas)........=> ' || count(*) Verifica_FormaPagto from t_rhstu_forma_pagamento;
select 'ConsultaFPagto(130 linhas)..=> ' || count(*) Verifica_ConsultaFormaPagto from t_rhstu_consulta_forma_pagto;

