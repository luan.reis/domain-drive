import React from 'react'

export default function Produto() {
    const {modelo, placa, cor} = JSON.parse(sessionStorage.getItem("carroObj"))
    return (
        <>
            <h1>Carro</h1>
            <p><b>Modelo:</b> {modelo}</p>
            <p><b>Cor:</b> {cor}</p>
            <p><b>Placa:</b> {placa}</p>
        </>

    )
}
