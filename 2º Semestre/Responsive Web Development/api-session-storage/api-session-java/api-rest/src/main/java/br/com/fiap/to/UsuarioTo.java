package br.com.fiap.to;

public class UsuarioTo {

	
	private String login;
	
	private String senha;
	
	
	

	public UsuarioTo() {
		super();
	}

	public UsuarioTo(String login, String senha) {
		super();
		this.login = login;
		this.senha = senha;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Override
	public String toString() {
		return "UsuarioTo [login=" + login + ", senha=" + senha + "]";
	}
	
	
	
	
	
	
}
