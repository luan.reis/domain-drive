package br.com.fiap.pratica07.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class view {

	public static void main(String[] args) {
		
//		Preencha um vetor do tipo double de 16 posições
//		utilizando os valores fornecidos pelo usuário e
//		depois troque os 8 primeiros valores pelos 8
//		últimos e vice-e-versa. Apresente ao final o vetor
//		obtido.
		
		Scanner sc = new Scanner(System.in);
		List<Double> nums = new ArrayList<>(16);
		List<Double> aux1 = new ArrayList<>();
		List<Double> aux2 = new ArrayList<>();
		
		for (int i = 0; i < 16; i++) {
			nums.add((double) (i + 1));
		}
		
		for (int c = 0; c < 8; c++) {
			aux1.add(nums.get(c));
		}
			
		for (int d = 15; d >= 8; d--) {
			aux2.add(nums.get(d));
		}
		nums.clear();
		nums.addAll(aux2);
		nums.addAll(aux1);
				
		System.out.println("Trocando a ordem");
		System.out.println(nums);
	}
}
