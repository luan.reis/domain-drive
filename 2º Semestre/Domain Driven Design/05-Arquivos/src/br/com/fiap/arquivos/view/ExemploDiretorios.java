package br.com.fiap.arquivos.view;

import java.io.File;
import java.io.IOException;

public class ExemploDiretorios {

	public static void main(String[] args) {
		
		//Criar um objeto que representa um diretorio (pasta) chamado pokedex
		File diretorio = new File("pokedex");
		
		//Validar se o diretorio existe e � um diretorio
		if (diretorio.isDirectory()) {
			//Exibir o caminho completo do diret�rio
			System.out.println(diretorio.getAbsolutePath());
			
			//Recuperar os arquivos e/ou diret�rios do diret�rio
			File[] files = diretorio.listFiles();
			
			
			//Percorrendo o vator de arquivos e diretorios
			int qtdArquivo = 0;
			int qtdDiretorio = 0;
			
			for (File f : files) {
				
				if(f.isFile()) {
					qtdArquivo++;
				}else {
					qtdDiretorio++;
				}
				System.out.println(f.getName());
			}
			
			
	
			//Exibir a quantidade de arquivos no diret�rio
			System.out.println("Qtd de arquivos: " + qtdArquivo);
			
			
			//Exibir a quantidade de diret�rios no diret�rio
			System.out.println("Qtd de diretorios: " + qtdDiretorio);
			
			//Criar um arquivo dentro do diret�rio
			File arquivo = new File(diretorio, "pikachu.txt");
			try {
				arquivo.createNewFile();
				System.out.println("Arquivo criado");
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
			
		} else {
			//Se nao existir, vamos criar o diretorio
			diretorio.mkdir();
			System.out.println("Diret�rio criado!");
		}
		
	}
}