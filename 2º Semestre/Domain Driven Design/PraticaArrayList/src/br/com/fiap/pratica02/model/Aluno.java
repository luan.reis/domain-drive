package br.com.fiap.pratica02.model;

import java.util.Arrays;

public class Aluno {
	
	private int rm;
	private String nome;
	private int idade;
	private double notas[] = new double[2];
	
	
	
	public Aluno(int rm, String nome, int idade, double[] notas) {
		super();
		this.rm = rm;
		this.nome = nome;
		this.idade = idade;
		this.notas = notas;
	}
	
	
	
	public int getRm() {
		return rm;
	}


	public void setRm(int rm) {
		this.rm = rm;
	}



	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public double[] getNotas() {
		return notas;
	}
	public void setNotas(double[] notas) {
		this.notas = notas;
	}
	
	
	
	public double media() {
		
		double media = notas[0];
		for (int i = 1; i < notas.length; i++) {
			
			media += notas[i];
		}
		
		return (media / notas.length);
		
	}



	@Override
	public String toString() {
		return "Aluno [ RM: " + rm +" Nome: " + nome + ", Idade: " + idade + ", Notas: " + Arrays.toString(notas) + " M�dia: " + media() + "]";
	}
	
	
	
	
	
	
	
	

}
