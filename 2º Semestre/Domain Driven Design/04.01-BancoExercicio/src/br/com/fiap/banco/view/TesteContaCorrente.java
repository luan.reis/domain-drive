package br.com.fiap.banco.view;

import br.com.fiap.banco.exception.ValorNaoPermitidoException;
import br.com.fiap.banco.exception.ValorNegativoException;
import br.com.fiap.banco.model.ContaCorrente;

public class TesteContaCorrente {

	public static void main(String[] args){
		// TODO Auto-generated method stub
		
		
		try {
			
			ContaCorrente cc = new ContaCorrente(100, 100);
			
			
			cc.setLimite(250);
			System.out.println("Limite alterado");
	
			
			cc.sacar(1500);
			System.out.println("Valor sacado");
			
			
			
			cc.depositar(150);
			System.out.println("Valor depositado");
			System.out.println("Saldo: R$" + cc.getSaldo() + '\n');

			System.out.println("Limite: R$" + cc.getLimite()  + '\n');
	
			
		}catch(ValorNegativoException | ValorNaoPermitidoException e) {
			System.err.println(e.getMessage());
		}
		


	}

}
