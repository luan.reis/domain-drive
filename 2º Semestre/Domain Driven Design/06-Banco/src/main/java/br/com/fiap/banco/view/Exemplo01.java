package br.com.fiap.banco.view;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.fiap.banco.factory.ConnectionFactory;

public class Exemplo01 {

	
	public static void main(String[] args) {
		
		//Conectar com o banco de dados
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
			Connection conn = ConnectionFactory.getConnection();
			
			
			System.out.println("Conectado");
			
			//Cadastrar um filme
			
			Statement stmt = conn.createStatement();
			int qtdLinhas = stmt.executeUpdate("insert into tdss_tb_filme "
					+ "values(sq_tdss_tb_filme.nextval, 'Mad Max', 120, to_date('18/08/2012','dd/mm/yyyy'),'ação')");
			
			System.out.println("Linhas afetadas: " + qtdLinhas);
			
			
			
			conn.close();
		}catch(ClassNotFoundException e) {
			System.out.println("O driver jdbc não foi encontrado");
			e.printStackTrace();
			
		}catch(SQLException e) {
			System.out.println("Não foi possivel conectar no banco de dados");
			e.printStackTrace();
			
		}
		
		
		
		
	}
	
	
	
}
