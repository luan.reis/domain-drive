package br.com.fiap.banco.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.fiap.banco.factory.ConnectionFactory;
import br.com.fiap.banco.model.Filme;
import br.com.fiap.exception.IdNotFoundException;

public class FilmeDao {
	
	
	public Connection criandoConexao() throws ClassNotFoundException, SQLException {
		
		Connection conn = ConnectionFactory.getConnection();
		
		return conn;
	}

	public Filme buscarPorId(int codigo) throws SQLException, IdNotFoundException, ClassNotFoundException {

		Connection conn = ConnectionFactory.getConnection();

		PreparedStatement stmt = conn.prepareStatement("select * from tdss_tb_filme where cd_fime = ?");
		stmt.setInt(1, codigo);
		ResultSet result = stmt.executeQuery();

		Filme filme;
		if (!result.next()) {
			throw new IdNotFoundException("Filme não encontrato");
		}

		int id = result.getInt("cd_fime");
		String nome = result.getString("nm_filme");
		int duracao = result.getInt("nr_minutos");
		String dt = result.getString("dt_lancamento");
		String genero = result.getString("ds_genero");
		filme = new Filme(id, nome, genero, duracao, dt);

		conn.close();
		return filme;

	}

	public void deletar(int codigo) throws ClassNotFoundException, SQLException, IdNotFoundException {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = conn.prepareStatement("DELETE FROM tdss_tb_filme WHERE cd_fime = ?");
		stmt.setInt(1, codigo);
		
		int qtd = stmt.executeUpdate();

		if(qtd == 0) {
			throw new IdNotFoundException("Id não encontrado");		}
		
	}

	public void atualizar(Filme filme) throws ClassNotFoundException, SQLException, IdNotFoundException {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = conn.prepareStatement("UPDATE tdss_tb_filme SET nm_filme = ?, nr_minutos = ?, dt_lancamento = to_date(?, 'dd/mm/yyyy'), ds_genero = ? where cd_fime = ?");
		stmt.setString(1, filme.getNome());
		stmt.setInt(2, filme.getDuracao());
		stmt.setString(3, filme.getDataLancamento());
		stmt.setString(4, filme.getGenero());
		stmt.setInt(5, filme.getCodigo());
		
		int qtd = stmt.executeUpdate();

		if(qtd == 0) {
			throw new IdNotFoundException("Filme não encontrado");
		}
		
		conn.close();
	}
	
	public List<Filme> listar() throws ClassNotFoundException, SQLException{
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = conn.prepareStatement("SELECT * FROM tdss_tb_filme order by cd_fime");
		ResultSet result = stmt.executeQuery();
		List<Filme> listaFilme = new ArrayList<Filme>();
		while(result.next()) {
			
			Filme filme = new Filme(
					result.getInt("cd_fime"),
					result.getString("nm_filme"),
					result.getString("ds_genero"),
					result.getInt("nr_minutos"),
					result.getString("dt_lancamento"));
			listaFilme.add(filme);
		}

		
		return listaFilme;
		
		
	}

	public void cadastrar(Filme filme) {

		try {
			Connection conn = ConnectionFactory.getConnection();
			PreparedStatement stmt = conn.prepareStatement(
					"insert into tdss_tb_filme " + "values(sq_tdss_tb_filme.nextval, ?, ?, to_date(?,'dd/mm/yyyy'), ?)",
					new String[] { "cd_fime" });

			stmt.setString(1, filme.getNome());
			stmt.setInt(2, filme.getDuracao());
			stmt.setString(3, filme.getDataLancamento());
			stmt.setString(4, filme.getGenero());
			stmt.executeUpdate();

			ResultSet result = stmt.getGeneratedKeys();
			if (result.next()) {
				int codigo = result.getInt(1);
				filme.setCodigo(codigo);
			}

			System.out.println("Filme cadastrado");

			conn.close();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
