package br.com.fiap.banco.model;

public class Filme {
	
	private int codigo;
	private String nome;
	private String genero;
	private int duracao;
	private String dataLancamento;
	
	
	public Filme() {
	
	}
	
	public Filme(int codigo, String nome, String genero, int duracao, String dataLancamento) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.genero = genero;
		this.duracao = duracao;
		this.dataLancamento = dataLancamento;
	}

	public Filme(String nome, String genero, int duracao, String dataLancamento) {
		super();
		this.nome = nome;
		this.genero = genero;
		this.duracao = duracao;
		this.dataLancamento = dataLancamento;
	}
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getDuracao() {
		return duracao;
	}

	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}

	public String getDataLancamento() {
		return dataLancamento;
	}

	public void setDataLancamento(String dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	@Override
	public String toString() {
		return "Filme codigo: " + codigo + ", nome: " + nome.toUpperCase() + ", genero: " + genero.toUpperCase() + ", duracao: " + duracao
				+ ", Data lancamento: " + dataLancamento.toUpperCase();
	}
	
	
	
	
	
	
	
	
	
}
