package br.com.fiap.banco.view;

import java.sql.SQLException;
import java.util.Scanner;

import br.com.fiap.banco.dao.FilmeDao;
import br.com.fiap.banco.model.Filme;
import br.com.fiap.exception.IdNotFoundException;

public class Exemplo04 {
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException, IdNotFoundException {
		
		Scanner sc = new Scanner(System.in);
		
		FilmeDao filmeDao = new FilmeDao();
		
		System.out.println("Informe o numero do ID do filme");
		int codigo = sc.nextInt();
		
		Filme filme = filmeDao.buscarPorId(codigo);
		
		System.out.println(filme.toString());
		
		
		
		
		
	}

}
