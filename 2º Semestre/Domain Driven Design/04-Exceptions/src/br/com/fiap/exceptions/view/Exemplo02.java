package br.com.fiap.exceptions.view;

import java.util.InputMismatchException;
import java.util.Scanner;

import br.com.fiap.exceptions.exception.XpInvalidoException;
import br.com.fiap.exceptions.exception.XpNegativoException;
import br.com.fiap.exceptions.model.Pokemon;

public class Exemplo02 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		//Ler o nome do pokemon
		System.out.println("Digite o nome do pokemon");
		String nome = sc.next() + sc.nextLine();
		
		//Instanciar o pokemon
		Pokemon poke = new Pokemon(nome);
		//Variavel auxiliar
		String continuar;
		
		do {
			try {
				//Ler o numero de xp que o pokemon vai ganhar
				System.out.println("Digite o xp do " + poke.getNome());
				int xp = sc.nextInt();
				
				//Aumentar o xp do pokemon
				poke.ganharXp(xp);
				
			} catch (XpNegativoException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			} catch(InputMismatchException e) {
				System.err.println("Insira um valor inteiro");
				sc.next();
			} catch(XpInvalidoException e) {
				System.err.println(e.getMessage());
			} 
			//Exibir o xp e o level do pokemon 
			System.out.println(poke.getNome() + " " 
					+ poke.getXp() + " xp" + " Level: " + poke.getLevel());
			
			//Verificar se deseja ganhar mais xp
			System.out.println("Deseja ganhar mais xp? S/N");
			continuar = sc.next() + sc.nextLine();
			
		} while(continuar.equalsIgnoreCase("S"));
		sc.close();
	}
}