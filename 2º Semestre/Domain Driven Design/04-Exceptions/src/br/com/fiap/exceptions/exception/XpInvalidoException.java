package br.com.fiap.exceptions.exception;

public class XpInvalidoException extends Exception {

	public XpInvalidoException(String msg) {
		super(msg);
	}
	
}