package br.com.pratica05;

import javax.swing.JOptionPane;

public class Viewer {

	public static void main(String[] args) {
	
//		Implemente um programa em Java que leia uma
//		sequencia de N números reais informados pelo
//		usuário e depois exiba esta sequencia na ordem
//		oposta à entrada pelo usuário. Exemplo: 1,4 2,65
//		3,89 4,005 exibir 4,005 3,89 2,65 1,4.
		
		
		
		int quantidade = Integer.parseInt(JOptionPane.showInputDialog("Quantidade de numeros que deseja incluir: "));
		double numeros[] = new double[quantidade];
		
		
		
		for(int i = 0; i < quantidade; i++) {
			numeros[i] = Double.parseDouble(JOptionPane.showInputDialog("Informe o numero: "));
			
		}
		
		for(int j = (quantidade - 1); j >= 0; j--) {
			System.out.print(numeros[j]);
			
		}
		
		
		

	}

}
