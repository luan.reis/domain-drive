 package br.com.pratica03.model;

import java.util.Arrays;
import java.util.Iterator;

public class Aluno {

	
	private String nome;
	private double notas[] = new double [3];
	
	

	public Aluno(String nome, double[] notas) {
		this.nome = nome;
		this.notas = notas;
		
		
		
	}
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double[] getNotas() {
		return notas;
	}
	public void setNotas(double[] notas) {
		this.notas = notas;
	}
	
	

	public double maior() {
		double maior = notas[0];
		for(int i = 1; i < notas.length; i++) {
			if(notas[i] > maior) {
				maior = notas[i];
			}
		}
		return maior;
	}
	
	public double media() {
		double media = 0;
		for(int i = 0; i < this.notas.length; i++) {
			media += this.notas[i];
		}
		media = (media / this.notas.length);
		
		return media;
		
	}
	
	
//	public String toString() {
//		return "Aluno [nome=" + nome + ", notas= [" + Arrays.toString(notas) + "]" + "Media:" + this.media()  + "]";
//	}
	
	@Override
	public String toString() {
		return "Aluno [nome=" + nome + ", notas= " + Arrays.toString(notas) + " Maior nota: " + this.maior() + "]";
	}
	
	
	
	
	
	
	
	
	
	
}
