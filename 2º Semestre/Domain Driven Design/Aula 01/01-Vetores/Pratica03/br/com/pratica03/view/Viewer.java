package br.com.pratica03.view;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.com.pratica03.model.Aluno;

public class Viewer {

	public static void main(String[] args) {
		String melhor = "";
		String nome;

		int quantidade = Integer.parseInt(JOptionPane.showInputDialog("Informe a quantidade de alunos"));
		Aluno[] aluno = new Aluno[quantidade];

		for (int i = 0; i < quantidade; i++) {
			double notas[] = new double[3];
			nome = JOptionPane.showInputDialog("Digite o nome do " + (i + 1) + " aluno: ");

			for (int j = 0; j < notas.length; j++) {
				notas[j] = Double.parseDouble(
						JOptionPane.showInputDialog("Digite a " + (j + 1) + " ª nota do " + (i + 1) + "º aluno: "));
			}

			aluno[i] = new Aluno(nome, notas);

		}

		for (int i = 1; i < quantidade; i++) {
			double maior = aluno[0].maior();			
			if(aluno[i].maior() > maior) {
				melhor = aluno[i].toString();
			}
		}
		
		System.out.println(melhor);

	}

}
