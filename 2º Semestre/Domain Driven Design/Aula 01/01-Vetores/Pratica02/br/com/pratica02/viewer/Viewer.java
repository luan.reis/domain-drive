package br.com.pratica02.viewer;

import javax.swing.JOptionPane;

public class Viewer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		Crie um programa em Java onde o usuário informa
//		N números e ao final é impresso o maior entre eles.
//		Nota: o número N é um valor solicitado ao usuário
//		durante a execução do programa; utilize vetores
//		na solução.
		
		
		int quantidade = Integer.parseInt(JOptionPane.showInputDialog("Informe a quantidade de numeros"));
		int array[] = new int [quantidade];
		int maior = 0;
		
		
		for(int i = 0; i < array.length; i++) {
			array[i] = Integer.parseInt(JOptionPane.showInputDialog("Informe o " + (i + 1) + "º numero:"  ));
			
			if(array[i] > maior) {
				maior = array[i];
			};
		}
		
		
		JOptionPane.showMessageDialog(null, "O maior numero digitado foi: " + maior);
		
		
		
		
		
		

	}

}
