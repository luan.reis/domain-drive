package br.com.fiap.vetores.view;

import javax.swing.JOptionPane;

public class Exemplo01 {

	public static void main(String[] banana) {
		//Declarar um vetor de nomes
		String[] nomes =  new String[5];

		//Adicionar os nomes no vetor (usuario digitar)
		for(int j = 0; j < nomes.length; j++) {
			nomes[j] =  JOptionPane.showInputDialog("Digite o " + (j + 1) + "º" + " nome");
			
		}
		
		
		// Criar um laço de repetição e exibir os nomes do vetor
		for(int i = 0; i < nomes.length; i++) {
			System.out.println((i + 1) + "º nome: " + nomes[i].toUpperCase());
		}
		
	
		
		
		//Criar um laço de repetição e exibir os nomes do vetor
		for(String nome : nomes) {
			System.out.println("nome: " + nome.toUpperCase());
			}
		
	}

}
